#include <iostream>
#include <list>
#include <vector>

#include "catch.hpp"

using namespace std;

template<typename Iterator>
void advance_it(Iterator &it, size_t n)
{
    if constexpr (is_same_v<typename iterator_traits<Iterator>::iterator_category,
                            random_access_iterator_tag>)
    {
        cout << "rand_access_iterator\n";
        it += n;
    }
    else
    {
        cout << "generic iterator\n";
        for (auto i{0u}; i < n; ++i)
            ++it;
    }
}

TEST_CASE("constexpr-if with iterator categories")
{
    SECTION("random_access_iterator")
    {
        SECTION("vector")
        {
            vector<int> data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

            auto it = data.begin();

            advance_it(it, 3);

            REQUIRE(*it == 4);
        }

        SECTION("array")
        {
            int tab[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

            auto it = begin(tab);

            advance_it(it, 3);

            REQUIRE(*it == 4);
        }
    }

    SECTION("input_iterator")
    {
        list<int> data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        auto it = data.begin();

        advance_it(it, 3);

        REQUIRE(*it == 4);
    }
}
