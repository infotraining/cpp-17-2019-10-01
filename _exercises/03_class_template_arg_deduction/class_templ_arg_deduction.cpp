#include <algorithm>
#include <iostream>
#include <list>
#include <numeric>
#include <optional>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template<typename T>
struct Holder
{
    T value;

    Holder(const T& v)
        : value(v)
    {
    }

    template<typename Args>
    Holder(initializer_list<Args> lst)
        : value(lst)
    {
    }
};

template<typename Args>
Holder(initializer_list<Args>)->Holder<vector<Args>>;

template<typename T>
Holder(T)->Holder<T>;

Holder(const char *)->Holder<string>;

///////////////////////////////////////////////////////////////////////////////////

TEST_CASE("Guess deduced argument types")
{
    int x = 42;
    int& ref_x = x;
    const int& cref_x = x;

    using TODO = void;

    Holder h1(x);
    static_assert(is_same_v<decltype(h1), Holder<int>>);

    Holder h2(ref_x);
    static_assert(is_same_v<decltype(h2), Holder<int>>);

    Holder h3(cref_x);
    static_assert(is_same_v<decltype(h3), Holder<int>>);

    int tab[10];
    Holder h4 = tab;
    static_assert(is_same_v<decltype(h4), Holder<int *>>);

    Holder h5("test");
    static_assert(is_same_v<decltype(h5), Holder<string>>);

    Holder h6{1, 2, 3, 4, 5, 6};
    static_assert(is_same_v<decltype(h6), Holder<vector<int>>>);

    Holder h7(h6);
    static_assert(is_same_v<decltype(h7), Holder<vector<int>>>);

    Holder h8{h6, h7};
    static_assert(is_same_v<decltype(h8), Holder<vector<Holder<vector<int>>>>>);
}

void foo(int, double) {}

TEST_CASE("CTAD in std::library")
{
    SECTION("pair")
    {
        pair<int, double> p1{1, 3.14};
        pair p2{1, 3.14};
    }

    SECTION("tuple")
    {
        tuple t1{1, 2.17, "text", "text"s}; // tuple<int, double, cons char*, string>
        tuple t2{t1};                       // tuple<int, double, cons char*, string>
    }

    SECTION("optional")
    {
        optional opt1 = 1;       // optional<int>
        optional opt2 = "text"s; // optional<string>
        optional opt3 = opt1;    // optional<int>
    }

    SECTION("function")
    {
        function<void(int, double)> f1 = foo;
        function f2 = &foo;
        f2(10, 3.14);
    }

    SECTION("smart ptrs")
    {
        unique_ptr<int[]> uptr1(new int[10]);
        shared_ptr<int> sptr1(new int(13));

        shared_ptr sptr2(std::move(uptr1)); // shared_ptr<int[]>
        weak_ptr wptr = sptr1;              // weak_ptr<int>
    }

    SECTION("containers")
    {
        vector vec = {1, 2, 3, 4}; // vector<int>
        vector vec2{vec};          // vector<int>

        list lst{1, 2, 3};                  // list<int>
        vector items(begin(lst), end(lst)); // vector<int>

        array arr = {1, 2, 3, 4}; // array<int, 4>
    }
}

template<typename T>
struct Generic
{
    T value;

    using Type = common_type_t<T>;

    Generic(Type v)
        : value(v)
    {
    }
};

template<typename T>
Generic(T)->Generic<enable_if_t<is_integral_v<T>, T>>;

TEST_CASE("disabling CTAD")
{
    Generic g1{1}; // Generic<int>
    //Generic g2 = "string"s;
}
