#include <iostream>
#include <memory>
#include <set>
#include <string>
#include <string_view>
#include <vector>
#include <algorithm>

#include "catch.hpp"

using namespace std;

/////////////////////////////////////////////////////////////////////////////////////////////////

template<typename Container, typename... Args>
long long int matches(const Container& container, const Args&... args)
{
    return (... + count(begin(container), end(container), args));
}

TEST_CASE("matches - returns how many items is stored in a container")
{
    vector<int> v{1, 2, 3, 4, 5};

    REQUIRE(matches(v, 2, 5) == 2);
    REQUIRE(matches(v, 100, 200) == 0);
    REQUIRE(matches("abcdef", 'x', 'y', 'z') == 0);
    REQUIRE(matches("abcdef", 'a', 'd', 'f') == 3);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
class Gadget
{
public:
    virtual std::string id() const { return "a"; }
    virtual ~Gadget() = default;
};

class SuperGadget : public Gadget
{
public:
    std::string id() const override { return "b"; }
};

/// Hint: Use std::common_type_t<Ts...>

template<typename... Args>
auto make_vector(Args&&... args)
{
    using ResultType = common_type_t<Args...>;

    vector<ResultType> vec;
    vec.reserve(sizeof...(args));
    (..., vec.emplace_back(forward<Args>(args)));

    return vec;
}

TEST_CASE("make_vector - create vector from a list of arguments")
{
    using namespace Catch::Matchers;

    SECTION("ints")
    {
        std::vector<int> v = make_vector(1, 2, 3);

        REQUIRE_THAT(v, Equals(vector{1, 2, 3}));
    }

    SECTION("unique_ptrs")
    {
        auto ptrs = make_vector(make_unique<int>(5), make_unique<int>(6));

        REQUIRE(ptrs.size() == 2);
    }

    SECTION("unique_ptrs with polymorphic hierarchy")
    {
        const auto gadgets = make_vector(make_unique<Gadget>(),
                                         make_unique<SuperGadget>(),
                                         make_unique<Gadget>());

        static_assert(is_same_v<decltype(gadgets)::value_type, unique_ptr<Gadget>>);

        vector<string> ids;
        transform(begin(gadgets), end(gadgets), back_inserter(ids), [](auto& ptr) {
            return ptr->id();
        });

        REQUIRE_THAT(ids, Equals(vector<string>{"a", "b", "a"}));
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
struct Range
{
    T low;
    T high;
};

template<typename T1, typename T2>
Range(T1, T2)->Range<common_type_t<T1, T2>>;

template<typename T, typename... Args>
bool within(const Range<T>& range, const Args&... args)
{
    //    auto is_within = [&range](const auto& item) { return item >= range.low && item <= range.high; };
    //    return (... && is_within(args));

    using CT = common_type_t<T, Args...>;
    return ((std::clamp<CT>(args, range.low, range.high) == args) && ...);
}

TEST_CASE("within - checks if all values fit in range [low, high]")
{
    REQUIRE(within(Range{10, 20.0}, 1, 15, 30) == false);
    REQUIRE(within(Range{10, 20}, 11, 12, 13) == true);
    REQUIRE(within(Range{5.0, 5.5}, 5.1, 5.2, 5.3) == true);
}

///////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
void hash_combine(size_t& seed, const T& value)
{
    seed ^= hash<T>{}(value) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

template<typename... Args>
size_t combined_hash(const Args&... args)
{
    size_t seed = 0;
    (..., hash_combine(seed, args));

    return seed;
}

TEST_CASE("combined_hash - write a function that calculates combined hash value for a given number "
          "of arguments")
{
    size_t seed{};

    REQUIRE(combined_hash(1U) == 2654435770U);
    REQUIRE(combined_hash(1, 3.14, "string"s) == 10365827363824479057U);
    REQUIRE(combined_hash(123L, "abc"sv, 234, 3.14f) == 162170636579575197U);
}

struct Person
{
    string first_name;
    string last_name;
    uint8_t age;

    auto tied() const { return std::tie(first_name, last_name, age); }

    bool operator==(const Person& p) const { return tied() == p.tied(); }

    bool operator<(const Person& p) const { return tied() < p.tied(); }
};

template<typename Tpl, size_t... Is>
size_t hash_for_tuple_impl(const Tpl& tpl, std::index_sequence<Is...>)
{
    return combined_hash(std::get<Is>(tpl)...);
}

template<typename... Ts>
size_t hash_for_tuple(const std::tuple<Ts...>& tpl)
{
    using Indexes = std::make_index_sequence<sizeof...(Ts)>;
    return hash_for_tuple_impl(tpl, Indexes{});
}

struct PersonHash
{
    size_t operator()(const Person& p) const { return hash_for_tuple(p.tied()); }
};
