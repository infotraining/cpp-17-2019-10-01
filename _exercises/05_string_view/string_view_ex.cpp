#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <string_view>
#include <vector>

#include "catch.hpp"

using namespace std;

vector<string_view> split_text(string_view text, string_view separators = " ,"sv)
{
    vector<string_view> tokens;

    for (auto it1 = cbegin(text); it1 != cend(text);)
    {
        auto it2 = find_first_of(it1, cend(text), cbegin(separators), cend(separators));

        auto index = it1 - cbegin(text);
        auto length = it2 - it1;
        tokens.emplace_back(&text[index], length);

        if (it2 == cend(text))
            break;

        it1 = next(it2);
    }

    return tokens;
}

TEST_CASE("split with spaces")
{
    string text = "one two three four";

    auto words = split_text(text);

    auto expected = {"one", "two", "three", "four"};

    REQUIRE(equal(begin(expected), end(expected), begin(words)));
}

TEST_CASE("split with commas")
{
    string text = "one,two,three,four";

    auto words = split_text("one,two,three,four"s);

    auto expected = {"one", "two", "three", "four"};

    REQUIRE(equal(begin(expected), end(expected), begin(words)));
}
