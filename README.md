# C++17

## Docs

* https://infotraining.bitbucket.io/cpp-17/


## Additonal information


### login and password for VM:

```
dev  /  pwd
```

### reinstall VBox addon (optional)

```
sudo /etc/init.d/vboxadd setup
```

### proxy settings

Add to `.profile` in user directory:

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=http://10.144.1.10:8080
```

```
export http_proxy=http://10.158.100.2:8080
export https_proxy=http://10.158.100.2:8080
```

### vcpkg settings

Add to `.profile`

```
export VCPKG_ROOT="/usr/share/vcpkg" 
export CC="/usr/bin/gcc-9"
export CXX="/usr/bin/g++-9"
```

## GIT repository

```
git clone https://infotraining-dev@bitbucket.org/infotraining/cpp-17-2019-10-01.git
```

## Ankieta

* https://www.infotraining.pl/ankieta/cpp-17-2019-10-01-kp
