#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template<typename T>
void deduce1(T arg)
{
    puts(__PRETTY_FUNCTION__);
}

void foo() {}

TEST_CASE("Template Argument Deduction - case 1")
{
    int x = 10;
    const int cx = 10;
    int &ref_x = x;
    const int &cref_x = cx;
    int tab[10];

    deduce1(x);
    auto a1 = x; // int

    deduce1(cx);
    auto a2 = cx; // int

    deduce1(ref_x);
    auto a3 = ref_x; // int

    deduce1(cref_x);
    auto a4 = cref_x; // int

    deduce1(tab);
    auto a5 = tab; // int*

    deduce1(foo);
    auto a6 = foo; // void(*)()
}

template<typename T>
void deduce2(T &arg)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("Template Argument Deduction - case 2")
{
    int x = 10;
    const int cx = 10;
    int &ref_x = x;
    const int &cref_x = cx;
    int tab[10];

    deduce2(x);
    auto &a1 = x; // int&

    deduce2(cx);
    auto &a2 = cx; // const int&

    deduce2(ref_x);
    auto &a3 = ref_x; // int&

    deduce2(cref_x);
    auto &a4 = cref_x; // const int&

    deduce2(tab);
    auto &a5 = tab; // int(&)[10]

    deduce2(foo);
    auto &a6 = foo; // void(&)()
}

template<typename T>
void deduce3(T &&arg)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("Template Argument Deduction - case 3")
{
    int x = 10;

    deduce3(x);
    auto &&a1 = x; // int&

    deduce3(10);
    auto &&a2 = 10; // int&&
}

template<typename T1, typename T2>
struct ValuePair
{
    T1 fst;
    T2 snd;

    ValuePair(const T1 &f, const T2 &s)
        : fst(f)
        , snd(s)
    {
    }

    ValuePair(const std::pair<T1, T2> &p)
        : fst(p.first)
        , snd(p.second)
    {
    }

    template<typename TArg>
    ValuePair(const TArg &arg)
        : fst(arg)
        , snd(arg)
    {
    }
};

// deduction guides
template<typename T1, typename T2>
ValuePair(T1, T2)->ValuePair<T1, T2>;

ValuePair(const char *, const char *)->ValuePair<string, string>;

template<typename TArg>
ValuePair(TArg)->ValuePair<TArg, TArg>;

TEST_CASE("Class Template Arg Deduction - CTAD")
{
    ValuePair<int, double> vp1{1, 3.14};

    ValuePair vp2(1, 3.14);
    ValuePair vp3{1, 3.14}; // ValuePair<int, double>
    //ValuePair<int> vp_illformed(1, 3.14);

    ValuePair vp4{"text"s, "text"}; // ValuePair<string, const char*>

    int tab[10];
    int &ref_first = tab[0];
    ValuePair vp5{tab, ref_first}; // ValuePair<int*, int>

    REQUIRE(vp5.fst == &tab[0]);

    pair p(1, "text"s);
    ValuePair vp6(p); // ValuePair<int, string>
    static_assert(is_same_v<decltype(vp6), ValuePair<int, string>>);

    SECTION("deduction using copy constructor")
    {
        ValuePair vp7{vp6};
        static_assert(is_same_v<decltype(vp7), ValuePair<int, string>>);
    }

    ValuePair vp8("text", "abc");
    static_assert(is_same_v<decltype(vp8), ValuePair<string, string>>);

    ValuePair vp9(3.14); // ValuePair<double, double>
    REQUIRE((vp9.fst == Approx(3.14) && vp9.snd == Approx(3.14)));

    ValuePair vp10(vp9); // ValuePair<double, double>
    static_assert(is_same_v<decltype(vp10), ValuePair<double, double>>);
}

template<typename T>
struct Data
{
    T value;

    Data(const T &v)
        : value(v)
    {
    }

    template<typename ItemType>
    Data(initializer_list<ItemType> il)
        : value(il)
    {
    }
};

template<typename T>
Data(T)->Data<T>;

template<typename T>
Data(initializer_list<T>)->Data<vector<T>>;

TEST_CASE("CTAD for Data")
{
    Data d1 = 1;
    static_assert(is_same_v<decltype(d1), Data<int>>);
    Data d2(1);
    static_assert(is_same_v<decltype(d2), Data<int>>);
    Data d3{1};
    static_assert(is_same_v<decltype(d3), Data<vector<int>>>);
    Data d4 = {1};
    static_assert(is_same_v<decltype(d4), Data<vector<int>>>);
    Data d5 = {1, 2};
    static_assert(is_same_v<decltype(d5), Data<vector<int>>>);
    Data d6{1, 2};
    static_assert(is_same_v<decltype(d6), Data<vector<int>>>);

    //    Data d7{d6};
    //    static_assert(is_same_v<decltype(d7), Data<vector<int>>>);

    Data d8(d6);
    static_assert(is_same_v<decltype(d8), Data<vector<int>>>);
}

template<typename T1, typename T2>
struct Aggregate
{
    T1 a;
    T2 b;
};

template<typename T1, typename T2>
Aggregate(T1, T2)->Aggregate<T1, T2>;

TEST_CASE("CTAD for aggregates")
{
    Aggregate agg1{1, 3.14};
    static_assert(is_same_v<decltype(agg1), Aggregate<int, double>>);
}
