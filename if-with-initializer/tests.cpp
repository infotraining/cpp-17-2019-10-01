#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <queue>

#include "catch.hpp"

using namespace std;

TEST_CASE("if with initializer")
{
    vector vec = { 1, 2, 3, 4, 5 };
    
    if (auto pos = find(begin(vec), end(vec), 3); pos != end(vec))
    {
        cout << "Item " << *pos << " has been found!\n";
    }
    else
    {
        assert(pos == end(vec));
    }
}

TEST_CASE("use case with mutex")
{
    queue<int> q;
    mutex mtx_q;

    q.push(42);
    int value;

    SECTION("Before C++17")
    {
        {
            lock_guard<mutex> lk{mtx_q}; // mtx_q.lock()

            if (! q.empty())
            {
                value = q.front();
                q.pop();
            }
        } // mtx_q.unlock();

        // ...
        REQUIRE(value == 42);
    }

    SECTION("Since C++17")
    {
        if (lock_guard lk{mtx_q}; !q.empty()) // mtx_q.lock() 
        {
            value = q.front();
            q.pop();
        }  // mtx_q.unlock()

        //...
        REQUIRE(value == 42);
    }
}

