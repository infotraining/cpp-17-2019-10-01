#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;


template <typename T>
string convert_to_string(const T& value)
{
    if constexpr (is_integral_v<T>)
    {
        return to_string(value);
    }
    else if constexpr (is_same_v<T, string>)
    {
        return value;
    }
    else
    {
        return string(value);
    }
}

TEST_CASE("constexpr if")
{
    SECTION("convert to string")
    {
        SECTION("from number")
        {
            REQUIRE(convert_to_string(42) == "42"s);
        }

        SECTION("from string")
        {
            REQUIRE(convert_to_string("text"s) == "text"s);
        }

        SECTION("from const char*")
        {
            REQUIRE(convert_to_string("c-text") == "c-text"s);
        }
    }
}

int foo(int x)
{
    return x;
}

template <typename T, size_t V>
struct MetaFunction
{   
    using type = T;
    static constexpr T value = V;
};

template <typename T, size_t V>
using MetaFunction_t = typename MetaFunction<T, V>::type;

template <typename T, size_t V>
constexpr T MetaFunction_v = MetaFunction<T, V>::value;


// trait IsVoid
template <typename T>
struct IsVoid
{
    static constexpr bool value = false;
};

template <>
struct IsVoid<void>
{
     static constexpr bool value = true;
};

// variable template
template <typename T>
constexpr bool IsVoid_v = IsVoid<T>::value;


TEST_CASE("traits description")
{
    REQUIRE(foo(42) == 42);

    static_assert(is_same<MetaFunction_t<int, 665>, int>::value);
    static_assert(MetaFunction_v<int, 665> == 665);

    using T = void;
    static_assert(IsVoid<T>::value);
}

template <typename T, size_t N>
auto process_array(T (&data)[N])
{
    if constexpr(N <= 255)
    {
        cout << "Processing small array\n";
        array<T, N> result;
        for(size_t i = 0; i < N; ++i)
            result[i] = data[i];
        return result;
    }
    else
    {
        return vector<T>(begin(data), end(data));
    }    
}

TEST_CASE("processing of arrays")
{
    int tab[64] = {1, 2, 3};
    auto data = process_array(tab);
    static_assert(is_same_v<decltype(data), array<int, 64>>);

    int buffer[1024] = {};
    auto vec = process_array(buffer);
    static_assert(is_same_v<decltype(vec), vector<int>>);
}