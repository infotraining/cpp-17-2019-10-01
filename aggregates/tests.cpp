#include <algorithm>
#include <array>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>
#include <future>

#include "catch.hpp"

using namespace std;

struct SimpleAggregate
{
    int a;
    double b;
    int tab[3];
    string name;

    void print() const
    {
        cout << "SimpleAggregate{ " << a << ", " << b << ", [ ";
        for (const auto& item : tab)
            cout << item << " ";
        cout << "], " << name << "}\n";
    }
};

TEST_CASE("aggregate")
{
    SimpleAggregate agg1 {1, 3.14, {1, 2, 3}, "agg1"};
    agg1.print();

    SimpleAggregate agg2 {1};
    agg2.print();

    SimpleAggregate agg3 {};
    agg3.print();

    SimpleAggregate agg4;
    agg4.print();

    SECTION("native arrays are aggregates")
    {
        int tab1[3];

        int tab2[3] = {}; // [0, 0, 0]

        int tab3[3] = {1, 2}; // [1, 2, 0]
    }
}

struct Empty
{
};

struct ComplexAggregate : Empty, SimpleAggregate, std::string
{
    vector<int> vec;
};

TEST_CASE("Aggregates with inheritance")
{
    ComplexAggregate agg1 {{} /*for Empty aggregate*/, {1, 3.14, {1, 2, 3}, "simple agg"}, {"text"}, {665, 667}};

    agg1.print();
    REQUIRE(agg1.size() == 4);
    REQUIRE(agg1.vec.size() == 2);

    static_assert(is_aggregate_v<decltype(agg1)>);

    std::array<int, 4> arr = {};
    static_assert(is_aggregate_v<decltype(arr)>);
}

struct WTF
{
    string name;

    WTF() = delete; // user-declared
};

TEST_CASE("WTF")
{
    WTF wtf {};
}

struct Data
{
    int a, b;
    
    Data(int a, int b)
        : a(a)
        , b(b)
    {
    }
};

TEST_CASE("C++11")
{
    Data data {1, 2};
}

void save_to_file(const string& file_name)
{
    //...
    cout << "Start " << file_name << endl;
    this_thread::sleep_for(1s);
    cout << "End " << file_name << endl;
}

TEST_CASE("WTF2")
{
    auto f1 = async(launch::async, save_to_file, "f.1");
    auto f2 = async(launch::async, save_to_file, "f.2");
    auto f3 = async(launch::async, save_to_file, "f.3");
    auto f4 = async(launch::async, save_to_file, "f.4");
}