#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    void print() { cout << "\n"; }

    template<typename Head, typename... Tail>
    void print(Head head, Tail... tail)
    {
        cout << head << " ";
        print(tail...);
    }

    template<typename Head>
    auto sum(Head head)
    {
        return head;
    }

    template<typename Head, typename... Tail>
    auto sum(Head head, Tail... tail)
    {
        return head + sum(tail...);
    }

} // namespace BeforeCpp17

namespace SinceCpp17
{
    //    template<typename Head, typename... Tail>
    //    void print(Head head, Tail... tail)
    //    {
    //        cout << head << " ";

    //        if constexpr (sizeof...(tail) > 0)
    //        {
    //            print(tail...);
    //        }
    //        else
    //        {
    //            cout << "\n";
    //        }
    //    }

    template<typename... Args>
    auto sum(Args... args)
    {
        return (... + args); // left fold (... + args)
    }

    template<typename... Ts>
    void print(const Ts &... args)
    {
        bool is_first = true;

        auto with_space = [&is_first](const auto &arg) {
            if (!is_first)
                cout << " ";
            is_first = false;
            return arg;
        };

        (cout << ... << with_space(args)) << "\n";
    }

    template<typename... Ts>
    void print_lines(const Ts &... args)
    {
        ((cout << args << " "), ...);
        cout << "\n";
    }

} // namespace SinceCpp17

TEST_CASE("variadic templates")
{
    SinceCpp17::print(1, 3.14, "text"s, "abc");

    REQUIRE(SinceCpp17::sum(1, 2, 3, 4) == 10);

    SinceCpp17::print_lines(1, 3.14, "text"s, "abc");
}
