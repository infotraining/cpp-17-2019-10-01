#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

vector<int> create_vector_rvo(size_t n)
{
    return vector<int>(n); // prvalue
}

vector<unsigned int> create_vector_nrvo(size_t n)
{
    vector<unsigned int> vec(n);

    for (auto i{0u}; i < n; ++i)
        vec[i] = i;

    return vec; // lvalue
}

TEST_CASE("rvo - copy elision")
{
    vector<int> vec = create_vector_rvo(1'000'000);
}

struct CopyMoveDisabled
{
    vector<int> data;

    CopyMoveDisabled() = default;
    CopyMoveDisabled(const CopyMoveDisabled &) = delete;
    CopyMoveDisabled &operator=(const CopyMoveDisabled &) = delete;
    CopyMoveDisabled(CopyMoveDisabled &&) = delete;
    CopyMoveDisabled &operator=(CopyMoveDisabled &&) = delete;
};

CopyMoveDisabled create_impossible()
{
    return CopyMoveDisabled{{1, 2, 3}};
}

TEST_CASE("noncopyable & nonmoveable can be returned from function")
{
    CopyMoveDisabled cp1 = create_impossible();
    REQUIRE_THAT(cp1.data, Catch::Matchers::Equals(vector{1, 2, 3}));
}

void use(const vector<int> &vec)
{
    REQUIRE(vec.size() == 3);
}

TEST_CASE("temporary materialization")
{
    use(create_vector_rvo(3));
}
