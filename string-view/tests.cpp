#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("string-view")
{
    const char* ctext = "abc def";
    string text = "ghi jkl";

    string_view sv1(ctext, 3);

    REQUIRE(sv1 == "abc"sv);

    SECTION("implicit conversion from c-string & std::string")
    {
        string_view sv2 = ctext;
        string_view sv3 = text;

        sv3.remove_prefix(4);

        REQUIRE(sv3 == "jkl"sv);
    }
}

template<typename Container>
void print_all(const Container& container, string_view prefix)
{
    cout << prefix << ": [ ";
    for (const auto& item : container)
        cout << item << " ";
    cout << "]\n";
}

TEST_CASE("printing")
{
    vector vec = {1, 2, 3};
    print_all(vec, "vec"s);
}

string_view get_prefix(string_view text)
{
    assert(text.size() >= 3);
    return {text.data(), 3};
}

TEST_CASE("beware")
{
    string_view sv = "text"s; // dangling pointer

    string text = "abcdef";
    string_view prefix = get_prefix(text);

    cout << prefix << "\n";

    prefix = get_prefix("text"s); // dangling pointer
    cout << prefix << "\n";
}

TEST_CASE("conversion from string_view")
{
    string_view sv = "text";

    const char* c_txt = sv.data();
    string txt(sv);

    char buffer[3] = {'a', 'b', 'c'};

    string_view sv2(buffer, 3);

    cout << sv2 << "\n";
}
