#include <algorithm>
#include <charconv>
#include <iostream>
#include <numeric>
#include <optional>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("optional")
{
    SECTION("construction")
    {
        optional<int> o1;
        REQUIRE(o1.has_value() == false);

        optional<int> empty{nullopt};
        REQUIRE(empty.has_value() == false);

        optional<int> o2 = 42;
        REQUIRE(o2.has_value());

        if (o2)
            cout << *o2 << endl;

        optional text = "abc"s; // optional<string>

        auto ctext = "def";
        optional<string_view> osv{in_place, ctext, 3};
    }

    SECTION("getting value")
    {
        optional non_empty = 42;
        optional<int> empty;

        SECTION("unsafe")
        {
            REQUIRE(*non_empty == 42);
            // REQUIRE(*empty); // UB
        }

        SECTION("safe")
        {
            REQUIRE(non_empty.value() == 42);
            REQUIRE_THROWS_AS(empty.value(), bad_optional_access);

            REQUIRE(non_empty.value_or(-1) == 42);
            REQUIRE(empty.value_or(-1) == -1);
        }
    }
}

optional<int> to_int(string_view str)
{
    int result{};

    auto start = str.data();
    auto end = start + str.size();

    if (auto [pos, error_code] = from_chars(start, end, result);
        error_code != std::errc{} || pos != end)
    {
        return nullopt;
    }

    return result;
}

TEST_CASE("to_int")
{
    string str1 = "42";
    REQUIRE(to_int(str1).value() == 42);

    auto text = "665a3a";
    REQUIRE(to_int(text).has_value() == false);

    optional<std::reference_wrapper<string>> opt_ref = str1;
    opt_ref.value().get() = "text";
}
