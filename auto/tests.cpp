#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("auto & {}")
{
    int x1 = 10;
    int x2{10};

    auto a1 = 10;   // int
    auto a2{10};    // int
    auto a3 = {10}; // initializer_list<int>

    for (auto i{0u}; i < 10; ++i)
        cout << i << " ";
    cout << "\n";
}

void foo()
{
    cout << "foo()\n";
}

void idle() {}

void bar() noexcept
{
    cout << "bar() noexcept\n";
}

TEST_CASE("noexcept")
{
    void (*ptr_fun)() = &foo;
    ptr_fun();

    ptr_fun = &bar;
    ptr_fun();

    static_assert(!is_same_v<decltype(foo), decltype(bar)>);
}

template<typename Callable1, typename Callable2>
void call(Callable1 f1, Callable2 f2)
{
    f1();
    f2();
}

struct Base
{
    virtual void print() noexcept { cout << "Base::print() noexcept" << endl; }
    virtual ~Base() = default;
};

struct Derived : Base
{
    void print() noexcept override { cout << "Derived::print() noexcept" << endl; }
};

TEST_CASE("call")
{
    call(foo, bar);
}
