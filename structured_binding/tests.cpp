#include <algorithm>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <string>
#include <tuple>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        vector<int>::const_iterator min, max;
        tie(min, max) = minmax_element(data.begin(), data.end());

        double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

        return make_tuple(*min, *max, avg);
    }
}

template <typename Container>
tuple<int, int, double> calc_stats(const Container& data)
{
    auto [min_pos, max_pos] = minmax_element(begin(data), end(data));

    double avg = accumulate(begin(data), end(data), 0.0) / size(data);

    return tuple(*min_pos, *max_pos, avg); // CTAD
}

TEST_CASE("Before C++17")
{
    vector<int> data = {4, 42, 665, 1, 123, 13};

    SECTION("using get<N>()")
    {
        tuple<int, int, double> stats = BeforeCpp17::calc_stats(data);

        REQUIRE(std::get<0>(stats) == 1); // min
        REQUIRE(std::get<1>(stats) == 665); // max
        REQUIRE(std::get<2>(stats) == Approx(141.333)); // avg
    }

    SECTION("using tie()")
    {
        int min, max;
        double avg;

        tie(min, max, avg) = BeforeCpp17::calc_stats(data);

        REQUIRE(min == 1);
        REQUIRE(max == 665);
        REQUIRE(avg == Approx(141.333));
    }
}

TEST_CASE("Since C++17")
{
    SECTION("data - vector")
    {
        vector<int> data = {4, 42, 665, 1, 123, 13};

        auto [min, max, avg] = BeforeCpp17::calc_stats(data);

        REQUIRE(min == 1);
        REQUIRE(max == 665);
        REQUIRE(avg == Approx(141.333));
    }

    SECTION("data - array")
    {
        int data[] = {4, 42, 665, 1, 123, 13};

        auto [min, max, avg] = calc_stats(data);

        REQUIRE(min == 1);
        REQUIRE(max == 665);
        REQUIRE(avg == Approx(141.333));
    }
}

auto get_coord() -> int (&)[3]
{
    static int pos[] = {1, 2, 3};

    return pos;
}

std::array<int, 3> get_velocity()
{
    return {1, 2, 3};
}

TEST_CASE("structured bindings")
{
    SECTION("native arrays")
    {
        double coord[] = {1.0, 2.0, 3.0};

        auto [x, y, z] = coord;

        REQUIRE(x == Approx(1.0));
        REQUIRE(y == Approx(2.0));
        REQUIRE(z == Approx(3.0));

        auto [pos_x, pos_y, pos_z] = get_coord();

        REQUIRE(pos_x == 1);
    }

    SECTION("std::array")
    {
        auto [vx, vy, vz] = get_velocity();

        REQUIRE(vx == 1);
        REQUIRE(vz == 3);
    }
}

struct Error
{
    int id;
    int error_code;
    string message;
};

[[nodiscard]] Error open_file()
{
    return Error {1, 13, "file not found"};
}

TEST_CASE("structure bindings - structs")
{
    auto [id, errc, msg] = open_file();

    REQUIRE(id == 1);
    REQUIRE(errc == 13);
    REQUIRE(msg == "file not found"s);
}

TEST_CASE("structure bindings - how it works")
{
    tuple t1 {1, 3.14, "pi"s};

    auto [id, pi, desc] = t1; // t1 is copied

    SECTION("is iterpreted as")
    {
        auto unnamed_object = t1; // t1 is copied to unnamed_object
        auto& id = get<0>(unnamed_object);
        auto& pi = get<1>(unnamed_object);
        auto& desc = get<2>(unnamed_object);
    }
}

TEST_CASE("structure bindings - &, const, volatile, alignas")
{
    vector<int> data = {1, 2, 3};

    SECTION("const auto&")
    {
        const auto& [min, max, avg] = calc_stats(data);
    }

    SECTION("auto&")
    {
        int tab[] = {1, 2};
        auto& [x, y] = tab; // no copy
    }

    SECTION("alignas")
    {
        alignas(64) auto [vx, vy, vz] = get_velocity();
    }
}

struct Data
{
    int x;
    int& ref_x;
};

TEST_CASE("beware")
{
    int x = 10;
    int& ref_x = x;

    auto ax1 = x; // int
    auto ax2 = ref_x; // int

    SECTION("broken deduction")
    {
        Data data{x, ref_x};

        auto [ax1, ax2] = data;

        static_assert(is_same_v<decltype(ax1), int>);
        static_assert(is_same_v<decltype(ax2), int&>);
    }
}

TEST_CASE("use cases")
{
    map<int, string> dict = { { 1, "one"}, {2, "two"}, {3, "three"} };

    SECTION("iteration over maps")
    {
        for(const auto& [key, value] : dict)
        {
            cout << key << " - " << value << endl;
        }
    }

    SECTION("insert into maps")
    {
        if (auto [pos, ok] = dict.insert(pair(4, "four")); ok)
        {
            const auto& [key, value] = *pos;
            cout << "Pair " << key << " - " << value << " was inserted...\n";
        }
        else
        {
            const auto& [key, value] = *pos;
            cout << "Key " << key << " is already in a map...\n";
        }
    }

    SECTION("init many vars")
    {
        auto [x, y, is_ok] = tuple(1, 3.14, true);

        list lst = {1, 2, 3};

        for(auto [index, it] = tuple(0, begin(lst)); it != end(lst); ++index, ++it)
        {
            cout << "item at " << index << " = " << *it << "\n";
        }
    }
}

enum Something { some, thing };
const map<int, string> something_desc = { {some, "some"}, {thing, "thing"} };

// step 1 - describe tuple-size of item
template <>
struct std::tuple_size<Something>
{
    static constexpr size_t value = 2;
};

// step 2 - describe types
template <>
struct std::tuple_element<0, Something>
{
    using type = int;
};

template <>
struct std::tuple_element<1, Something>
{
    using type = std::string;
};

// step 3 - how to get to n-th element
template <size_t N>
decltype(auto) get(const Something&);

template <>
decltype(auto) get<0>(const Something& sth)
{
    return static_cast<int>(sth);
}

template <>
decltype(auto) get<1>(const Something& sth)
{
    return something_desc.at(sth);
}

TEST_CASE("structure bindings with enum")
{
    Something sth = some;

    const auto [value, desc] = sth;

    REQUIRE(value == 0);
    REQUIRE(desc == "some"s);
}