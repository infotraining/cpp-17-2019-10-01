#include <algorithm>
#include <any>
#include <iostream>
#include <numeric>
#include <string>
#include <unordered_map>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("any")
{
    std::any a1;

    REQUIRE(a1.has_value() == false);

    a1 = 1;
    a1 = 3.14;
    a1 = "text"s;
    a1 = vector{1, 2, 3};

    SECTION("any_cast")
    {
        vector<int> vec = any_cast<vector<int>>(a1);

        REQUIRE(vec == vector{1, 2, 3});

        REQUIRE_THROWS_AS(any_cast<string>(a1), bad_any_cast);
    }

    SECTION("any_cast with pointers")
    {
        vector<int>* ptr_vec = any_cast<vector<int>>(&a1);

        REQUIRE(ptr_vec != nullptr);
        REQUIRE(*ptr_vec == vector{1, 2, 3});

        auto string_ptr = any_cast<string>(&a1);
        REQUIRE(string_ptr == nullptr);
    }

    SECTION("any usues RTTI")
    {
        const type_info& info = a1.type();
        cout << info.name() << "\n";
    }
}

struct KeyValueDictionary
{
    unordered_map<string, any> pairs;

    void insert(string key, any value) { pairs.emplace(move(key), move(value)); }

    template<typename T>
    T& at(const string& key)
    {
        T* value = any_cast<T>(&(pairs.at(key)));

        if (!value)
            throw bad_any_cast{};

        return *value;
    }
};

TEST_CASE("KeyValueDictionary")
{
    KeyValueDictionary dict{{{"surname", "Kowalski"s}, {"id", 665}}};
    dict.insert("name"s, "Jan"s);
    dict.insert("age"s, 42);

    REQUIRE(dict.at<string>("name") == "Jan"s);
    REQUIRE(dict.at<int>("age") == 42);

    dict.at<string>("name") = "Adam";

    REQUIRE(dict.at<string>("name") == "Adam"s);
}
