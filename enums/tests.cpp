#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

enum class Coffee : uint8_t
{
    espresso,
    latte,
    americano
};

TEST_CASE("enums")
{
    Coffee c = Coffee::espresso;

    SECTION("since C++17")
    {
        Coffee coffee{1}; // now allowed

        REQUIRE(coffee == Coffee::latte);
    }
}

enum class Index : uint32_t
{
};

constexpr size_t to_size_t(Index index)
{
    return static_cast<size_t>(index);
}

struct Array
{
    vector<int> items;

    int &operator[](Index index) { return items[to_size_t(index)]; }
};

TEST_CASE("using Array with Index")
{
    Array arr{{1, 2, 3}};
    REQUIRE(arr[Index{1}] == 2);
}

TEST_CASE("std::byte")
{
    std::byte b1{0b0010};

    //b1 += 24; // nonsense

    b1 <<= 2;
    b1 = ~b1 & std::byte{2};

    std::cout << std::to_integer<unsigned int>(b1) << std::endl;
}
