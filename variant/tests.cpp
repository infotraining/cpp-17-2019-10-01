#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <variant>
#include <vector>

#include "catch.hpp"

using namespace std;

struct NoDefaultConstructor
{
    int value;

    NoDefaultConstructor() = delete;
};

TEST_CASE("variant")
{
    variant<int, double, string> v1;

    REQUIRE(holds_alternative<int>(v1));
    REQUIRE(get<int>(v1) == 0);

    v1 = 3.14;
    v1 = "text"s;
    v1 = "abc";

    SECTION("getting values")
    {
        REQUIRE(get<string>(v1) == "abc"s);
        REQUIRE_THROWS_AS(get<int>(v1), bad_variant_access);

        REQUIRE(get_if<string>(&v1) != nullptr);
        REQUIRE(*get_if<string>(&v1) == "abc"s);
        REQUIRE(get_if<int>(&v1) == nullptr);
    }

    SECTION("emplace")
    {
        v1.emplace<int>(1);
        v1.emplace<string>("text");

        variant<int, double, int> v3;
        v3.emplace<0>(42);
        v3.emplace<2>(665);

        REQUIRE(get<2>(v3) == 665);
    }

    SECTION("monostate")
    {
        variant<monostate, NoDefaultConstructor, int, string> v2;
        REQUIRE(holds_alternative<monostate>(v2));
    }

    SECTION("beware of conversions")
    {
        variant<string, bool> v_strange = "text"s;

        REQUIRE(v_strange.index() == 0);
    }
}

struct Printer
{
    void operator()(int x) const
    {
        cout << "int(" << x << ")\n";
    }

    void operator()(double d) const
    {
        cout << "double(" << d << ")\n";
    }

    void operator()(const string& s) const
    {
        cout << "string(" << s << ")\n";
    }

    void operator()(const vector<int>& vec) const
    {
        cout << "vector<int>( ";
        for (const auto& item : vec)
            cout << item << " ";
        cout << ")\n";
    }
};

template<typename... Lambdas>
struct Overloader : Lambdas...
{
    using Lambdas::operator()...;
};

template<typename... Lambdas>
Overloader(Lambdas...)->Overloader<Lambdas...>;

TEST_CASE("visiting variants")
{
    variant<int, double, string, vector<int>> v = 3.14;

    v = vector{1, 2, 3};

    Printer prn;
    visit(prn, v);

    auto printer = Overloader{[](int i) { cout << "i(" << i << ")\n"; },
                              [](double d) { cout << "d(" << d << ")\n"; },
                              [](const string& str) { cout << "s(" << str << ")\n"; },
                              [](const vector<int>& v) { cout << "v(" << v.size() << ")\n"; }};

    visit(printer, v);
}

struct ErrorCode
{
    string message;
    errc error_code;
};

[[nodiscard]] variant<string, ErrorCode> load_from_file(const string& file_name)
{
    if (file_name == "nie ma"s)
    {
        return ErrorCode{"blad: 13", errc::bad_file_descriptor};
    }

    return "content"s;
}

TEST_CASE("handling result or errors")
{
    visit(Overloader{[](const string& s) { cout << s << "\n"; },
                     [](const ErrorCode& ec) { cout << "Error: " << ec.message << "\n"; }},
          load_from_file("nie ma"));
}
